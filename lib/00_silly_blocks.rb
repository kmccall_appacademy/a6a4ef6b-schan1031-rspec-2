def reverser
  yield.split.map(&:reverse).join(' ')
end

def adder(n = 1)
  yield + n
end

def repeater(x = 0)
  if x == 0
    return yield
  else
    x.times do |n|
      yield
    end
  end
end
