def measure(n = 0)
  if n.zero?
    start = Time.now
    yield
    fin = Time.now
    net = fin - start
  else
    vals = []
    n.times do |n|
      start = Time.now
      yield
      fin = Time.now
      net = fin - start
      vals.push(net)
    end
    vals.reduce(&:+)/vals.size
  end
end
